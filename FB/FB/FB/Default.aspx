﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Flexibele Deeltijd - NHL Stenden Hogeschool</title>
    <link href="css/StyleLogin.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="content">
              <table align="center" width="90%">
                  <tr>
                    <td>
                        <img src="Images/NHLStenden.png" width="110%" />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="username" placeholder="Gebruikersnaam" width="100%">
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="password" placeholder="Wachtwoord" width="100%"><br />
                    </td>
                  </tr>
                  <tr>
                    <th>
                        <input type="submit" value="Login">
                    <th>
                  </tr>
                  
              </table>
            </div>
        </div>
    </form>
</body>
</html>
